FROM alpine:latest

RUN apk add --no-cache \
    rsync \
    openssh \
    nano \
    openrc \
    curl \
    php84 \
    php84-common \
    php84-fpm \
    php84-pdo \
    php84-opcache \
    php84-zip \
    php84-phar \
    php84-iconv \
    php84-cli \
    php84-curl \
    php84-openssl \
    php84-mbstring \
    php84-tokenizer \
    php84-fileinfo \
    php84-json \
    php84-xml \
    php84-xmlwriter \
    php84-simplexml \
    php84-dom \
    php84-pdo_mysql \
    php84-pdo_sqlite \
    php84-tokenizer \
    php84-bcmath \
    php84-intl \
    php84-pecl-redis

RUN rm -rf /usr/bin/php
RUN ln -s /usr/bin/php84 /usr/bin/php

RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN rm -rf composer-setup.php

ENV COMPOSER_CACHE_DIR=composer-cache
ENV PATH="${PATH}:/usr/local/bin"

RUN curl -LO https://deployer.org/deployer.phar
RUN mv deployer.phar /usr/local/bin/dep
RUN chmod +x /usr/local/bin/dep

RUN rc-update add sshd

RUN mkdir -p ~/.ssh
